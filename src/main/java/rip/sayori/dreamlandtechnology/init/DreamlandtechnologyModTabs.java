
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package rip.sayori.dreamlandtechnology.init;

import rip.sayori.dreamlandtechnology.DreamlandtechnologyMod;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.core.registries.Registries;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class DreamlandtechnologyModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, DreamlandtechnologyMod.MODID);

	@SubscribeEvent
	public static void buildTabContentsVanilla(BuildCreativeModeTabContentsEvent tabData) {

		if (tabData.getTabKey() == CreativeModeTabs.BUILDING_BLOCKS) {
			tabData.accept(DreamlandtechnologyModBlocks.ORIGINIUM_BLOCK.get().asItem());
			tabData.accept(DreamlandtechnologyModBlocks.ORIGINIUM_ORE.get().asItem());
		}

		if (tabData.getTabKey() == CreativeModeTabs.INGREDIENTS) {
			tabData.accept(DreamlandtechnologyModItems.ORIGINIUM_INGOT.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.COLORED_BLOCKS) {
			tabData.accept(DreamlandtechnologyModBlocks.MACHINE_FRAME.get().asItem());
		}
	}
}
