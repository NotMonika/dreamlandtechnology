
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package rip.sayori.dreamlandtechnology.init;

import rip.sayori.dreamlandtechnology.item.OriginiumIngotItem;
import rip.sayori.dreamlandtechnology.DreamlandtechnologyMod;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BlockItem;

public class DreamlandtechnologyModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, DreamlandtechnologyMod.MODID);
	public static final RegistryObject<Item> ORIGINIUM_BLOCK = block(DreamlandtechnologyModBlocks.ORIGINIUM_BLOCK);
	public static final RegistryObject<Item> ORIGINIUM_ORE = block(DreamlandtechnologyModBlocks.ORIGINIUM_ORE);
	public static final RegistryObject<Item> ORIGINIUM_INGOT = REGISTRY.register("originium_ingot", () -> new OriginiumIngotItem());
	public static final RegistryObject<Item> MACHINE_FRAME = block(DreamlandtechnologyModBlocks.MACHINE_FRAME);

	private static RegistryObject<Item> block(RegistryObject<Block> block) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()));
	}
}
