
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package rip.sayori.dreamlandtechnology.init;

import rip.sayori.dreamlandtechnology.block.OriginiumOreBlock;
import rip.sayori.dreamlandtechnology.block.OriginiumBlockBlock;
import rip.sayori.dreamlandtechnology.block.MachineFrameBlock;
import rip.sayori.dreamlandtechnology.DreamlandtechnologyMod;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

public class DreamlandtechnologyModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, DreamlandtechnologyMod.MODID);
	public static final RegistryObject<Block> ORIGINIUM_BLOCK = REGISTRY.register("originium_block", () -> new OriginiumBlockBlock());
	public static final RegistryObject<Block> ORIGINIUM_ORE = REGISTRY.register("originium_ore", () -> new OriginiumOreBlock());
	public static final RegistryObject<Block> MACHINE_FRAME = REGISTRY.register("machine_frame", () -> new MachineFrameBlock());
}
